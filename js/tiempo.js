function  getTiempo() {
	
	
	fetch("https://weatherservices.herokuapp.com/api/weather")

	.then(data => data.json())
	.then(data => { var location = data['items'][0];
		var weather = location.weather;
		var forecast = Object.values(data['items'][0].forecast.forecast);
		mostrarTiempo(weather);
		mostrarTiempoActual(weather,location);
		mostrarLocalizacion(location);
		mostrarPronosticoExtendido(forecast);

	})

}

function getPronostico(){
	
	
}

function getDivTiempo(itemsTiempo){
	return `
	<span class="navbar-text">
	<h4 float: left;>
	<span class="badge badge-primary " >
	<div class="marquee">
	<p>`+itemsTiempo.description+`.</p>
	</div>	
	`+iconoSegunClima(itemsTiempo.id)+` `+itemsTiempo.temp+`ºC
	</span>
	</h4>
	</span>`
}

function getDivTiempoActual(itemsTiempo,localizacion){
	return`	<p style=" font-size:30px;"><i style="color:#64A3F1;" class="fas fa-map-marker-alt"></i> `+localizacion.name+`, `+localizacion.province+`</p>
			<div>
				<div class="card-body" style="width: 50rem;">
					<div class="row justify-content-start">
						<div class="col col-4" >
							<img src="img/clima/dom.svg" alt="clima" />
						</div>
						<div class="col col-5 align-self-end">
							<p class="text-muted"style="font-size:78px;">`+itemsTiempo.temp+` ºC</p>
						</div>	
					</div>
					<div class="card-group ">
						<div class="card">
							<div class="card border-light font-weight-normal ">
								<ul class="list-group list-group-flush">
									<li class="list-group-item list-group-item-dark"><b>`+itemsTiempo.description+`</b></li>
									<li class="list-group-item"><b>Presion H.:</b> `+itemsTiempo.pressure+` hPa</li>
									<li class="list-group-item list-group-item-dark"><b>Viento:</b> `+itemsTiempo.wing_deg+` a `+itemsTiempo.wind_speed+` km/h</li>
								</ul>
							</div>
						</div>
						<div class="card">
							<div class="card border-light">
								<ul class="list-group list-group-flush">
									<li class="list-group-item"><b>Visibilidad:</b> `+itemsTiempo.visibility+` km</li>
									<li class="list-group-item list-group-item-dark"><b>Humedad:</b> `+itemsTiempo.humidity+` %</li>
									<li class="list-group-item">...</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>`
}

function getDivPronosticoExtendido(pronostico){
	var retorno="";
	pronostico.forEach(element =>retorno = retorno + getCardPronostico(element));
	return`
	<div class="card-group" >
		`+retorno+`
	</div>`
}

function getCardPronostico(pronosticoDia){
	return `<div class="card" style="width: 18rem;">
		  <div class="card-body">
		    <a style="font-size:50px" class="text-primary">`+pronosticoDia.temp_max+`º</a>
		    <astyle="font-size:30px" class="text-muted">/`+pronosticoDia.temp_min+`º</a>
		    <h5 class="text-warning">`+diaSemana(pronosticoDia.date)+`</h5>
		    <h5 class="card-title">`+formatoFecha(pronosticoDia.date)+`</h5>
		  
		  <ul class="list-group list-group-flush text-body">
		    <li class="list-group-item"><b>Mañana:</b> `+iconoSegunClima(pronosticoDia.morning.weather_id)+`
		    <li class="list-group-item text-muted">`+pronosticoDia.morning.description+`</li>
		    <li class="list-group-item"><b>Tarde:</b> `+iconoSegunClima(pronosticoDia.afternoon.weather_id)+`
		    <li class="list-group-item text-muted">`+pronosticoDia.afternoon.description+`</li>
		  </ul>
		  </div>
		</div>` 
}
function getLocalizacion(localizacion){
	return `<p style=" font-size:30px;"><i style="color:#64A3F1;" class="fas fa-map-marker-alt"></i> `+localizacion.name+`, `+localizacion.province+`</p>`
}

function mostrarTiempo(itemsTiempo){

	var d1 = document.getElementById('tiempo');
	d1.insertAdjacentHTML('beforeEnd',getDivTiempo(itemsTiempo));

}

function mostrarTiempoActual(itemsTiempo,localizacion){
	var d1 = document.getElementById('tiempoActual');
	d1.insertAdjacentHTML('beforeEnd',getDivTiempoActual(itemsTiempo,localizacion));
}

function mostrarPronosticoExtendido(itemsTiempo){
	var d1 = document.getElementById('pronosticoExtendido');
	d1.insertAdjacentHTML('beforeEnd',getDivPronosticoExtendido(itemsTiempo));

}

function mostrarLocalizacion(localizacion){
	var d1 = document.getElementById('localizacion');
	d1.insertAdjacentHTML('beforeend',getLocalizacion(localizacion));

}

function iconoSegunClima(idClima){
	
	if(idClima === 0){
		return `<i class="fas fa-sun"></i>`;
	}else if(idClima === 1){
		return `<i class="fas fa-cloud-sun"></i>`
	}else if(idClima === 2,idClima === 3,idClima === 4){
		return `<i class="fas fa-cloud"></i>`
	}else if(idClima === 5){
		return `<i class="fas fa-wind"></i>`
	}else{
		return `<i class="fas fa-cloud-sun-rain"></i>`
	}
	
}

function diaSemana(fecha){
	var fechaEnCadena=fecha.split("-");
    var dias=["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    var dt = new Date(fechaEnCadena[1]+' '+fechaEnCadena[2]+', '+fechaEnCadena[0]+' 12:00:00');
    return dias[dt.getUTCDay()]   
}

function formatoFecha(fecha){
	var fechaEnCadena=fecha.split("-");
    return fechaEnCadena[2]+"/"+fechaEnCadena[1] 
}



//$(getPronostico)
$(document).ready(function(){
	getTiempo();
});