
//Guarda comercios en localstorage como string json
localStorage.setItem("comercios", JSON.stringify(comercios_jsonstr));
localStorage.setItem("usuarios", JSON.stringify(usuarios_jsonstr));
localStorage.setItem("mascotas", JSON.stringify(mascotas_jsonstr));
localStorage.setItem("avisos", JSON.stringify(avisos_jsonstr));

var arr_comercios = JSON.parse(localStorage.getItem("comercios"));
var arr_usuarios = JSON.parse(localStorage.getItem("usuarios"));
var arr_mascotas = JSON.parse(localStorage.getItem("mascotas"));
var arr_avisos = JSON.parse(localStorage.getItem("avisos"));

var filtrados = [];

function getCardAviso(aviso) {
	var mascota = getMascotaPorID(aviso.id_mascota);
	var usuario = getUsuarioPorID(aviso.id_usuario);

	var organizacion = '<i class="fa fa-ban p5" aria-hidden="true"></i>Sin organización';
	if (usuario.representa_org) {
		organizacion = '<i class="fa fa-check p5" aria-hidden="true"></i>'+usuario.nombre_org;
	}
	var anios_str = 'años';
	if (mascota.edad<=1) {
		anios_str = 'año';
	}
	var color_header = 'bg-warning';
	if (aviso.tipo == 'ENCONTRADO') {
		color_header = 'bg-success';
	}
	if (aviso.tipo == 'PERDIDO') {
		color_header = 'bg-danger';
	}

	return	`<div class="separator-bar"></div>	
			      	<div class="card text-center">

						<div class="card-header text-header-comercio `+color_header+`">
							`+aviso.tipo+` &nbsp;&nbsp;-&nbsp;&nbsp;<i class="fa fa-calendar-check-o p5" aria-hidden="true"></i>`+aviso.fecha+`   	
							<button class="btn btn-light float-right" onclick="toggleDivAviso('div-aviso`+aviso.id+`')">Ver/Ocultar detalles</button>
						</div>
						<div id="div-aviso`+aviso.id+`" class="card-body" style="display: none;">
							<div class="row justify-content-center">
						        <div class="col-md-3 col-sm-3">
			                        <div class="card" style="width: 18rem; height: 500px;">
									  <img src="img/usuarios/usuario`+usuario.id+`.jpg" class="card-img-top" alt="" style="width: 286px; height: 179px;">
									  <div class="card-body">
									    <h5 class="card-title">`+usuario.nombre+`</h5>
									    <p class="card-text">`+usuario.tipo+`</p>
									  </div>
									  <ul class="list-group list-group-flush">
									    <li class="list-group-item"><i class="fa fa-envelope-o p5" aria-hidden="true"></i>`+usuario.email+`</li>
									    <li class="list-group-item"><i class="fa fa-phone p5" aria-hidden="true"></i>`+usuario.telefono+`</li>
									    <li class="list-group-item">`+organizacion+`</li>
									  </ul>
									  <div class="card-body">
									    <a href="mailto:`+usuario.email+`" class="btn btn-primary"><i class="fa fa-envelope-o p5" aria-hidden="true"></i>Enviar email</a>
									    
									  </div>
									</div>
						        </div>
						        <div class="col-md-9 col-sm-9">
						           	<div class="card" style="width: 80%; height: 500px; display: inline-block;">
									  <div class="card-body">
									  	<div class="foto-mascota">
									  		<img src="img/mascotas/mascota`+mascota.id+`_0.jpg" class="card-img-top" alt="">
									  	</div>
									    <h5 class="card-title">`+mascota.nombre+`</h5>
				
									    <p class="card-text">`+aviso.tipo+`</p>
									    
									  </div>
									  <div class="text-left p25">
										    <h5>Raza: `+mascota.raza+`</h5>
										    <h6>Edad: `+mascota.edad+` `+anios_str+`</h6>
											<br>
										    <p class="texto-ni"><b>Detalles mascota:</b> `+mascota.detalles+`</p>
										    <br>
										    <p class="texto-ni"><b>Dirección:</b> `+aviso.direccion+`</p>
										    <p class="texto-ni"><b>Localidad:</b> `+aviso.localidad+`</p>
										    <br>
										    <p class="texto-ni"><b>Detalles aviso:</b> `+aviso.detalles+`</p>
									    </div>
									</div>

						        </div>
						    </div>
						</div>
						<hr/>
						<div class="card-body">
							<div class="foto-1 border">
								<img src="img/mascotas/mascota`+mascota.id+`_1.jpg" class="card-img-top" style="width:200px; height:200px;" alt="">
						    </div>
						    <div class="foto-3 border">
						    	<img src="img/mascotas/mascota`+mascota.id+`_3.jpg" class="card-img-top" style="width:200px; height:200px;" alt="">
						    </div>
						    <div class="foto-2 border">
						    	<img src="img/mascotas/mascota`+mascota.id+`_2.jpg" class="card-img-top" style="width:200px; height:200px;" alt="">
						    </div>
						    
						</div>
						<div class="card-footer text-muted">
						    <i class="fa fa-user p5 text-success" aria-hidden="true"></i> `+usuario.nombre+`&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paw p5 text-danger" aria-hidden="true"></i> `+mascota.nombre+`, `+mascota.raza+`&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-calendar p5 text-primary" aria-hidden="true"></i> `+mascota.edad+` `+anios_str+`
						</div>
					</div>`
}

function getSliderComercios() {
	var patrocinadores = [];
	var slider_patrocinadores = "";
	for (var i = 0; i < arr_comercios.length; i++) {
	   	if (arr_comercios[i].patrocinador) {
	       	slider_patrocinadores += `<div class="carousel-item">
		            <img class="" src="img/comercios/comercio`+arr_comercios[i].id+`.jpg" alt="Espacio publicitario disponible">
		            <div class="container">
		              <div class="carousel-caption texto-con-borde">
		                <h1>`+arr_comercios[i].nombre+`</h1>
		                <p>`+arr_comercios[i].rubro+`</p>
		                <p><a class="btn btn-lg btn-primary" href="mailto:`+arr_comercios[i].email+`" role="button">Enviar email al comercio</a></p>
		              </div>
		            </div>
		          </div>`
	   	}
	}
	return slider_patrocinadores;
}					

function getCardBuscador() {
	var tipos = "<option>Todos</option>";
	tipos += "<option>ADOPCIÓN</option>";
	tipos += "<option>ENCONTRADO</option>";
	tipos += "<option>PERDIDO</option>";
	

	return	`<div class="separator-bar"></div>	
			      	<div class="card text-center">

						<div class="card-header bg-info text-header-comercio">
						    	Buscador de avisos
						</div>
						
						<div class="card-body">
						<div class="container">
						    <div class="row justify-content-center">
						            <div class=" text-left texto-comercio">
										<div class="form-group">
										   <label for="aviso_buscado">Datos aviso</label>
										   <input type="email" class="form-control" id="aviso_buscado" placeholder="Ingrese búsqueda">
										</div>
									   	<div class="form-group">
										    <label for="group_tipo">Tipo de aviso</label>
										    <select class="form-control" id="group_tipo">
										      `+tipos+`
										    </select>
										</div>
										<button id="boton_buscador" class="btn btn-primary btn-sm btn-block" type="button" onclick="mostrarAvisosBuscados();"><i class="fa fa-search p5" aria-hidden="true"></i>Buscar</button>
										<button id="boton_limpiar" style="margin-bottom: 15px;" class="btn btn-warning btn-sm btn-block" type="button" onclick="limpiarFiltros();"><i class="fa fa-eraser p5" aria-hidden="true"></i></i>Limpiar filtros</button>
								    </div>
						    </div>
						</div>


						
						</div>
						<div class="card-footer text-muted">
						    Registrate en la comunidad y publicá tus avisos!
						</div>	
						</div>
						
					</div>`
}

function getCardBusquedaVacia() {

	return	`<div class="separator-bar"></div>	
			      	<div class="card text-center">
						<div class="card-body">
						    <div class=" text-left texto-comercio">
								No se encontraron comercios con las características buscadas.
						    </div>
						</div>
					</div>`
}

function agregaCardBuscador() {
	var d1 = document.getElementById('card_buscador');
	d1.insertAdjacentHTML('beforeend', getCardBuscador());
}

function mostrarAvisosBuscados() {
	var string_buscado = document.getElementById('aviso_buscado').value;
	var tipo_buscado = document.getElementById('group_tipo').value;

	filtrados = [];
	filtrados = arr_avisos.filter(aviso => aviso.detalles.toLowerCase().includes(string_buscado.toLowerCase()));
	if (tipo_buscado != "Todos") {
		filtrados = filtrados.filter(aviso => aviso.tipo === tipo_buscado);	
	}
	

	limpiarDivAvisos();
	if (filtrados.length==0) {
		mostrarBusquedaVacia();
	} else {
		mostrarAvisos(filtrados);
	}
	
}

function limpiarDivAvisos() {
	document.getElementById("lista_avisos").innerHTML="";
}

function mostrarSliderComercios() {
	var divUltimoSlide = document.getElementById('fin-slides');
	divUltimoSlide.insertAdjacentHTML('afterend', getSliderComercios());
}

function toggleDivAviso(div) {
	var x = document.getElementById(div);
  	if (x.style.display === "none") {
    	x.style.display = "block";
  	} else {
    	x.style.display = "none";
  	}
}

function agregaAvisoAListado(aviso) {
	var d1 = document.getElementById('lista_avisos');
	d1.insertAdjacentHTML('beforeend', getCardAviso(aviso));
}

function mostrarAvisos(arreglo_avisos) {
	arreglo_avisos.forEach(element => agregaAvisoAListado(element));
}

function mostrarBusquedaVacia() {
	var d1 = document.getElementById('lista_avisos');
	d1.insertAdjacentHTML('beforeend', getCardBusquedaVacia());
}

function getMascotaPorID(id) {
	return arr_mascotas.find(x => x.id === id);
}

function getUsuarioPorID(id) {
	return arr_usuarios.find(x => x.id === id);
}

function limpiarFiltros() {
	document.getElementById('aviso_buscado').value = "";
	document.getElementById('group_tipo').value = "Todos";
	limpiarDivAvisos();
	mostrarAvisos(arr_avisos);
}

$(document).ready(function(){
	mostrarSliderComercios();
	agregaCardBuscador();
	mostrarAvisos(arr_avisos);
});