var mascotas_jsonstr = [
      {
        "id": 1,
        "id_duenio": 1,
        "nombre": "Atila",
        "raza": "Golden Retriever",
        "edad": 3,
        "detalles": "Collar rojo, chapa identificatoria con su nombre grabado.",
      },
      {
        "id": 2,
        "id_duenio": 2,
        "nombre": "Canela",
        "raza": "Beagle",
        "edad": 2,
        "detalles": "Tiene perdida de visión, no ve muy bien.",
      },
      {
        "id": 3,
        "id_duenio": 3,
        "nombre": "Maia",
        "raza": "Mestizo",
        "edad": 7,
        "detalles": "Debe tomar remedios para el corazón todos los días.",
      },
      {
        "id": 4,
        "id_duenio": 4,
        "nombre": "Thor",
        "raza": "Pastor Alemán",
        "edad": 1,
        "detalles": "Muy juguetón y torpe.",
      },
      {
        "id": 5,
        "id_duenio": 5,
        "nombre": "Tony",
        "raza": "Caniche toy",
        "edad": 5,
        "detalles": "Tiene una pata lastimada.",
      },
      {
        "id": 6,
        "id_duenio": 6,
        "nombre": "Gomita",
        "raza": "Mestizo",
        "edad": 6,
        "detalles": "Lleva puesto collar rosa.",
      },
    ];
