var avisos_jsonstr = [
      {
        "id": 1,
        "id_usuario": 1,
        "id_mascota": 4,
        "tipo": "ADOPCIÓN",
        "activo": true,
        "fecha": "17/05/2020",
        "direccion": "Sanabria 2560",
        "localidad": "Malvinas Argentinas",
        "detalles": "Tiene todas las vacunas y está desparasitado." 
      },
      {
        "id": 2,
        "id_usuario": 2,
        "id_mascota": 3,
        "tipo": "PERDIDO",
        "activo": true,
        "fecha": "26/05/2020",
        "direccion": "Pampa 125",
        "localidad": "Malvinas Argentinas",
        "detalles": "Se perdió el día 25/06/2020 cerca de la plaza del barrio." 
      },
      {
        "id": 3,
        "id_usuario": 4,
        "id_mascota": 5,
        "tipo": "ENCONTRADO",
        "activo": true,
        "fecha": "27/05/2020",
        "direccion": "San Lorenzo 4320",
        "localidad": "San Miguel",
        "detalles": "Tiene un collar celeste con una chapa identificatoria que dice TONY." 
      }
    ];
