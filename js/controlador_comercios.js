
//Guarda comercios en localstorage como string json
localStorage.setItem("comercios", JSON.stringify(comercios_jsonstr));

var arr_comercios = JSON.parse(localStorage.getItem("comercios"));
var filtrados = [];
var arr_rubros = [];

var mapa_buscador;

var marker_patrocinador = L.icon({
    iconUrl: 'img/marker-patrocinador.png',
    shadowUrl: 'img/marker-shadow.png',

    iconSize:     [25, 41],
	iconAnchor:  [12, 41],
	popupAnchor: [1, -34],
	tooltipAnchor: [4, 62],
	shadowSize:  [-3, -76]
});

var marker_usuario = L.icon({
    iconUrl: 'img/marker-usuario.png',
    shadowUrl: 'img/marker-shadow.png',

    iconSize:     [25, 41],
	iconAnchor:  [12, 41],
	popupAnchor: [1, -34],
	tooltipAnchor: [4, 62],
	shadowSize:  [-3, -76]
});

//OPCIONES COORDENADAS JS
var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};

//GUARDA ARREGLO RUBROS
for (var i = 0; i < arr_comercios.length; i++) {
	   	if (!arr_rubros.includes(arr_comercios[i].rubro)) {
	       	arr_rubros.push(arr_comercios[i].rubro);
	   	}
}
arr_rubros.sort();

function getCardBuscador() {
	var rubros = "<option>Todos</option>";
	for (var i = 0; i < arr_rubros.length; i++) {
	   	rubros += "<option>"+arr_rubros[i]+"</option>";
	}

	return	`<div class="separator-bar"></div>	
			      	<div class="card text-center">

						<div class="card-header bg-info text-header-comercio">
						    	Buscador de comercios
						</div>
						
						<div class="card-body">
						<div class="container">
						    <div class="row justify-content-center">
						        <div class="col-md-6 col-sm-6">
						            <div class=" text-left texto-comercio">
										<div class="form-group">
										   <label for="comercio_buscado">Nombre del comercio</label>
										   <input type="email" class="form-control" id="comercio_buscado" placeholder="Ingrese nombre">
										</div>
									   	<div class="form-group">
										    <label for="group_rubro">Rubro del comercio</label>
										    <select class="form-control" id="group_rubro">
										      `+rubros+`
										    </select>
										</div>
										<div class="form-check">
										   <input type="checkbox" class="form-check-input" id="check_patrocinadores">
										   <label class="form-check-label" for="check_patrocinadores">Solo patrocinadores</label>
										</div><br>
										<button id="boton_buscador" class="btn btn-primary btn-sm btn-block" type="button" onclick="mostrarComerciosBuscados();"><i class="fa fa-search p5" aria-hidden="true"></i>Buscar</button>
										<button id="boton_limpiar" style="margin-bottom: 15px;" class="btn btn-warning btn-sm btn-block" type="button" onclick="limpiarFiltros();"><i class="fa fa-eraser p5" aria-hidden="true"></i></i>Limpiar filtros</button>
										<button id="boton_donde" style="margin-bottom: 15px;" class="btn btn-danger btn-sm btn-block" type="button" onclick="navigator.geolocation.getCurrentPosition(agregarMarcadorUsuario);"><i class="fa fa-map-marker p5" aria-hidden="true"></i>¿Dónde estoy?</button>
										
								    </div>
						        </div>
						        <div class="col-md-6 col-sm-6">
						            <div id="mapa-buscador" class="mapa-buscador">
						        </div>
						    </div>
						</div>


						
						</div>

						</div>
						<div class="card-footer text-muted">
						    ¿Sos comerciante de la comunidad? Tu comercio puede aparecer en el listado!
						</div>
					</div>

					`
}

function getCardBusquedaVacia() {

	return	`<div class="separator-bar"></div>	
			      	<div class="card text-center">
						<div class="card-body">
						    <div class=" text-left texto-comercio">
								No se encontraron comercios con las características buscadas.
						    </div>
						</div>
					</div>`
}

function getCardComercio(comercio) {
	var img_patrocinador = "";
	var txt_patrocinador = "";
	var bg_header = "bg-primary";
	if(comercio.patrocinador) {
		img_patrocinador = '<img class="img-patrocinador" src="img/patrocinador.png" alt="Patrocinador">';
		txt_patrocinador = '<h6 class="text-white bg-danger">PATROCINADOR DE LA COMUNIDAD</h5>';
		bg_header = "bg-warning";
	}
	return	`<div class="separator-bar"></div>	
			      	<div class="card text-center">

						<div class="card-header `+bg_header+` text-header-comercio">
						    	`+img_patrocinador+comercio.nombre+

						`<button class="btn btn-light float-right" onclick="toggleDiv('div-com`+comercio.id+`')">Ver/Ocultar detalles</button> </div>
						<div id="div-com`+comercio.id+`" class="card-body" style="display: none;">
							<div class="foto-comercio">
						    	<img src="img/comercios/comercio`+comercio.id+`.jpg" alt="`+comercio.nombre+`" class="img-thumbnail foto-comercio">
						    </div>
						    
						    <div id="map`+comercio.id+`" class="mapa-comercio">
						    	
						    </div>
						    <div class=" text-left texto-comercio">
							    <h5>`+comercio.nombre+`</h5>
							    <h6>`+comercio.rubro+`</h5>
								`+txt_patrocinador+`
								<br>
							    <p class="texto-ni"><b>Dirección:</b> `+comercio.direccion+`</p>
							    <p class="texto-ni"><b>Localidad:</b> `+comercio.localidad+`</p>
							    <p class="texto-ni"><b>Teléfono:</b> `+comercio.telefono+`</p>
							    <p class="texto-ni"><b>Email:</b> `+comercio.email+`</p>
							    <a href="mailto:`+comercio.email+`" class="btn btn-primary"><i class="fa fa-envelope-o p5" aria-hidden="true"></i>Comunicarse con el comercio</a>
						    </div>
						    
						</div>
						<div class="card-footer text-muted">
						    <i class="fa fa-circle p5 text-success" aria-hidden="true"></i> `+comercio.rubro+`&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-map-marker p5 text-danger" aria-hidden="true"></i> `+comercio.direccion+`, `+comercio.localidad+`&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-phone p5 text-primary" aria-hidden="true"></i> `+comercio.telefono+`
						</div>
					</div>`
}

function getSliderComercios() {
	var patrocinadores = [];
	var slider_patrocinadores = "";
	for (var i = 0; i < arr_comercios.length; i++) {
	   	if (arr_comercios[i].patrocinador) {
	       	slider_patrocinadores += `<div class="carousel-item">
		            <img class="" src="img/comercios/comercio`+arr_comercios[i].id+`.jpg" alt="Espacio publicitario disponible">
		            <div class="container">
		              <div class="carousel-caption texto-con-borde">
		                <h1>`+arr_comercios[i].nombre+`</h1>
		                <p>`+arr_comercios[i].rubro+`</p>
		                <p><a class="btn btn-lg btn-primary" href="mailto:`+arr_comercios[i].email+`" role="button">Enviar email al comercio</a></p>
		              </div>
		            </div>
		          </div>`
	   	}
	}
	return slider_patrocinadores;
}					

function agregaComercioAListado(comercio) {
	var d1 = document.getElementById('lista_comercios');
	d1.insertAdjacentHTML('beforeend', getCardComercio(comercio));
}

function mostrarComercios(arreglo_comercios) {
	arreglo_comercios.forEach(element => agregaComercioAListado(element));
}

function mostrarBusquedaVacia(comercio) {
	var d1 = document.getElementById('lista_comercios');
	d1.insertAdjacentHTML('beforeend', getCardBusquedaVacia());
}

function mostrarComerciosBuscados() {
	var string_buscado = document.getElementById('comercio_buscado').value;
	var rubro_buscado = document.getElementById('group_rubro').value;
	var busca_patrocinadores = document.getElementById('check_patrocinadores').checked;
	
	filtrados = [];
	filtrados = arr_comercios.filter(comercio => comercio.nombre.toLowerCase().includes(string_buscado.toLowerCase()));
	if (rubro_buscado != "Todos") {
		filtrados = filtrados.filter(comercio => comercio.rubro === rubro_buscado);	
	}
	if (busca_patrocinadores) {
		filtrados = filtrados.filter(comercio => comercio.patrocinador == true);	
	}
	

	limpiarDivComercios();
	if (filtrados.length==0) {
		mostrarBusquedaVacia();
	} else {
		mostrarComercios(filtrados);
		cargarDivsMapas(filtrados);		
	}
	mostrarMapaBuscador(filtrados);
	
	
}

function mostrarSliderComercios() {
	var divUltimoSlide = document.getElementById('fin-slides');
	divUltimoSlide.insertAdjacentHTML('afterend', getSliderComercios());
}

function limpiarDivComercios() {
		document.getElementById("lista_comercios").innerHTML="";
}

function cargarDivsMapas(comercios) {
	for (var i = 0; i < comercios.length; i++) {
		
		var loc_comercio = [comercios[i].coordenadas[0], comercios[i].coordenadas[1]];
		var mapa_comercio = L.map("map"+comercios[i].id).setView(loc_comercio, 14);
		var marker = L.marker(loc_comercio).addTo(mapa_comercio);
		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    	}).addTo(mapa_comercio);
	}
}

function agregaCardBuscador() {
	var d1 = document.getElementById('card_buscador');
	d1.insertAdjacentHTML('beforeend', getCardBuscador());
}


function agregarMarcadorUsuario(pos) {
	var crd = pos.coords;


		var loc_comercio = [crd.latitude, crd.longitude];
		var marker = L.marker(loc_comercio, {icon: marker_usuario}).addTo(mapa_buscador);
		marker.bindPopup("<b>Esta es tu ubicación</b>").openPopup();
		
}

function mostrarMapaBuscador(comercios) {
	var d1 = document.getElementById("mapa-buscador");
	d1.innerHTML="";
	var card_mapa =	`
						    <div id="mapa_comunidad" class="mapa-comu">
						    </div>
						    Mostrando `+comercios.length+` comercios según su búsqueda (`+(arr_comercios.length-comercios.length)+` ocultos).
					`;

	d1.insertAdjacentHTML('beforeend', card_mapa);

	mapa_buscador = L.map("mapa_comunidad").setView([-34.536052, -58.714961], 13);

	for (var i = 0; i < comercios.length; i++) {
		var loc_comercio = [comercios[i].coordenadas[0], comercios[i].coordenadas[1]];
		if (comercios[i].patrocinador) {
			var marker = L.marker(loc_comercio, {icon: marker_patrocinador}).addTo(mapa_buscador);
		} else {
			var marker = L.marker(loc_comercio).addTo(mapa_buscador);
		}
		
		var img_com = '<img width="160px" src="img/comercios/comercio'+comercios[i].id+'.jpg" alt="">';
		marker.bindPopup("<b>"+comercios[i].nombre+"</b><br>"+comercios[i].rubro+"<br>"+comercios[i].direccion+"<br>"+img_com);//.openPopup();
		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    	}).addTo(mapa_buscador);
	}
	
}

function toggleDiv(div) {
	var x = document.getElementById(div);
  	if (x.style.display === "none") {
    	x.style.display = "block";
  	} else {
    	x.style.display = "none";
  	}
}



//FUNCIONES FILTRO
function checkRubro(comercio, rubro) {
  return comercio.rubro === rubro;
}

function limpiarFiltros() {
	document.getElementById('comercio_buscado').value = "";
	document.getElementById('group_rubro').value = "Todos";
	document.getElementById('check_patrocinadores').checked = false;
	limpiarDivComercios();
	mostrarMapaBuscador(arr_comercios)
	mostrarComercios(arr_comercios);
	cargarDivsMapas(arr_comercios);
}


$(document).ready(function(){
	mostrarSliderComercios();
	agregaCardBuscador();
	mostrarMapaBuscador(arr_comercios);
	mostrarComercios(arr_comercios);
	cargarDivsMapas(arr_comercios);
});